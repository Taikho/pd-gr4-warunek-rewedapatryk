package pl.edu.uwm.wmii.rewedapatryk.laboratorium00;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Exercise 1
        Scanner scanner = new Scanner(System.in);
        int n = 0, lesser = 0, more = 0, eq = 0;
        try {
            System.out.println("How many numbers do You want draw ? : ");
            n = scanner.nextInt();

        } catch (Exception e) {
            System.out.println("It must be integer number more than 0 !! ");
            System.exit(1);
        }
        int[] array = new int[n];

        for(int i = 0; i < n; i++) {
            array[i] = getRandomNumb(-10, 10);
            if(array[i] > 5)
                more++;
            else if(array[i] == -5)
                eq++;
            else if(array[i] < -5)
                lesser++;
            System.out.println(array[i]);
        }
        System.out.println("lesser than -5: " + lesser + " equals -5: " + eq + " more than 5: " + more);

        //Exercise 2

        System.out.println(delete("aerobik", 'c'));
        System.out.println(delete2("aerobik", 'c'));


    }
// to Exercise 1
        public static int getRandomNumb(int min, int max)
        {
            Random random = new Random();
            return random.nextInt(max - min ) + min;
        }
// to Exercise 2
        public static String delete(String str, char c)
        {
            String withoutVowels;
            withoutVowels = str.replaceAll( "[aeiou]", String.valueOf(c));
            return withoutVowels;
        }
// Exercise 2 in other way i did this after time 5 minutes
        public static String delete2(String str, char c)
        {
            char[] tab =str.toCharArray();

            for (int i = 0; i < tab.length; i++)
            {
                if (tab[i]=='e'|| tab[i]=='a'|| tab[i]=='o'|| tab[i]=='u'|| tab[i]=='i')
                {
                    tab[i]= c;
                }
            }

            String str2 = Arrays.toString(tab);
            return str2;
        }



}