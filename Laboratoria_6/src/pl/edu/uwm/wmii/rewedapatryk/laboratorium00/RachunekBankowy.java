package pl.edu.uwm.wmii.rewedapatryk.laboratorium00;


 public class RachunekBankowy {
    static double rocznaStopaProcentowa;
    private int saldo;

    void obliczMiesieczneOdsetki() {
        double miesieczne;
        miesieczne = saldo * rocznaStopaProcentowa / 12;
        System.out.println("Miesieczne odsetki " + miesieczne);
        this.saldo += miesieczne;


    }

    static void setRocznaStopaProcentowa(double rocznaStopaProcentowa1) {
        rocznaStopaProcentowa = rocznaStopaProcentowa1;
    }

    public static void main(String[] args) {

        RachunekBankowy saver1 = new RachunekBankowy();
        saver1.saldo = 2000;

        RachunekBankowy saver2 = new RachunekBankowy();
        saver2.saldo = 3000;
        RachunekBankowy.rocznaStopaProcentowa = 0.04;
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.saldo);
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.saldo);



    }
}