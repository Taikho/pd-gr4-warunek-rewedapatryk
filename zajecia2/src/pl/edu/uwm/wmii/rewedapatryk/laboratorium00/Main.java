package pl.edu.uwm.wmii.rewedapatryk.laboratorium00;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Rozdzial Iteracyjne wczytywanie danych: ");

        System.out.println("Podaj ilosc liczb (n) : ");
        int n = scan.nextInt();
        double[] sequence = new double[n];
        for(int i = 0; i < n; i++) {
            try {
                System.out.println("Podaj wartość liczby numer " + (i + 1) + ": ");
                sequence[i] = scan.nextDouble();
            } catch (Exception e) {
                System.out.println("Use commas and only numbers");
            }
        }
            double pkta = 0, pktb =1, pktc = 0, pktd = 0, pkte = 1, pktf = 0, pkth = 0, pkti = 0;
            for (int i = 0; i < n; i++)
            {
               pkta += sequence[i];
               pktb *= sequence[i];
               pktc += Math.abs(sequence[i]);
               pktd += Math.sqrt(sequence[i]);
               pkte *= Math.abs(sequence[i]);
               pktf += Math.pow(sequence[i], 2);
               if(i % 2 == 0)
               {
                   pkth += sequence[i];

               }
               else
               {
                   pkth -= sequence[i];

               }

                if(i+1 > 1)
                {
                    int fact = 1;
                    for(int j=1;j <= i+1;j++)
                        fact=fact*j;
                    pkti += (Math.pow(-1, i+1) * sequence[i])/fact;

                }
                else
                {
                    pkti += (Math.pow(-1, i+1) * sequence[i])/1;
                }

            }

            System.out.println("Zadanie 1: ");
            System.out.println("a) " + pkta);
            System.out.println("b) " + pktb);
            System.out.println("c) " + pktc);
            System.out.println("d) " + pktd);
            System.out.println("e) " + pkte);
            System.out.println("f) " + pktf);
            System.out.println("g) " + pkta + " oraz " +  pktb);
            System.out.println("h) " + pkth);
            System.out.println("i) " + pkti);

           System.out.println("Zadanie 2: ");
           double[] sequenceZad2 = new double[n];
           double a1 = sequence[0];

           for(int i = 0; i < n; i ++ )
           {
               if(i < n-1) {
                   sequenceZad2[i] = sequence[i + 1];
               }
           else
               sequenceZad2[i] = a1;

           System.out.println(sequenceZad2[i]);
           }

        System.out.println("\n\nPołaczenie instrukcji iteracyjnej i warunkowej: ");
        System.out.println("Zad1: ");

        int odd = 0;  //nieparzyste
        int pktb2 = 0, pktc2 = 0, pktd2 = 0, pkte2 = 0, pktf2 = 0, pktg2 = 0, pkth2 = 0;

        for (int i = 0; i < n; i++) {

            int fact = 1;
            for(int j=1;j <= i+1;j++)
                fact=fact*j;

            if(sequence[i] % 2 != 0)
                odd ++;

            if(sequence[i] % 3 == 0 || sequence[i] % 5 != 0)
                pktb2++;

            if(Math.pow(sequence[i], 2) % 2 == 0)
                pktc2++;
            if( 1 < i && i < n-1 ) {
                if (sequence[i] < (sequence[i - 1] + sequence[i + 1])/2 )
                    pktd2++;
            }
            if( 1 <= i && i <= n ) {
                if (Math.pow(2, i) < sequence[i] && sequence[i] < fact)
                    pkte2++;
            }
            if(i+1 % 2 != 0 && sequence[i] % 2 == 0 )
                pktf2++;

            if(sequence[i] >= 0 && sequence[i] % 2 != 0 )
                pktg2++;

            if(Math.abs(sequence[i]) < Math.pow(i, 2))
                pkth2++;

        }

        System.out.println("a) " + odd);
        System.out.println("b) " + pktb2);
        System.out.println("c) " + pktc2);
        System.out.println("d) " + pktd2);
        System.out.println("e) " + pkte2);
        System.out.println("f) " + pktf2);
        System.out.println("g) " + pktg2);
        System.out.println("h) " + pkth2);

        System.out.println("Zad2: ");

        int dSum = 0;

        for (int i = 0; i < n; i++) {
            if (sequence[i] > 0)
                dSum+= 2 * sequence[i];
        }
        System.out.println("podwojna suma" + dSum);

        System.out.println("Zad3: ");
        int dod = 0, uj = 0, zero = 0;

        for (int i = 0; i < n; i++) {
            if (sequence[i] > 0)
                dod++;
            else if(sequence[i] < 0)
                uj++;
            else
                zero++;
        }

        System.out.println("ilość liczb dodatnich: " + dod + "ilość liczb ujemnych: " + uj + "ilość zer: " + zero);

        System.out.println("Zad4: ");

            double low = sequence[0];
            double max = sequence[0];

            for(int i = 1; i < sequence.length; i++) {
                if(sequence[i] > max) max = sequence[i];
                else if (sequence[i] < low) low = sequence[i];
            }
            System.out.println("Largest: " + max);
            System.out.println("Smallest: " + low);

        System.out.println("Zad5: ");
        for(int i = 0; i < n-1; i++) {
            if(sequence[i] > 0 && sequence[i+1] > 0)
                System.out.println("Para: " + sequence[i] + " " + sequence[i+1]);
        }

    }
}
