package pl.imiajd.Reweda;
import java.time.LocalDate;

public class TestOsoba {

    public static void main(String[] args) {
        Osoba[] ludzie = new Osoba[1];

       ludzie[0] = new Pracownik("Kowalski", new String[]{"Piotrek"}, LocalDate.of(1996, 4, 11), Osoba.Gender.MALE, 2000, LocalDate.of(2012, 5, 11));


         //ludzie[0] = new Osoba("Dyl");

          for (Osoba p : ludzie) {
              System.out.println(p.getBirthDay() + ": " + p.getOpis());
        }
    }
}
