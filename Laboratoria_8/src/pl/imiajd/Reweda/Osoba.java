package pl.imiajd.Reweda;
import java.time.LocalDate;

abstract class Osoba
{
    public Osoba(String nazwisko, String[] name, LocalDate birthDay, Gender gender)
    {
        this.nazwisko = nazwisko;
        this.name = name;
        this.birthDay = birthDay;
        this.gender = gender;

    }


    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String[] getName() {
        return name;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public Gender gender() {
        return gender;
    }
    public enum Gender {
        MALE, FEMALE
    }

    private String nazwisko;
    private String[] name;
    private LocalDate birthDay;

    private Gender gender;

}
