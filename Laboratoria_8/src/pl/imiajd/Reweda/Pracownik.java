package pl.imiajd.Reweda;

import java.time.LocalDate;

class Pracownik extends Osoba
{


    public Pracownik(String nazwisko, String[] name, LocalDate birthDay, Gender gender, double pobory, LocalDate dataZatrudnienia) {
        super(nazwisko, name, birthDay,gender);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }


    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    private double pobory;
    private LocalDate dataZatrudnienia;

}
