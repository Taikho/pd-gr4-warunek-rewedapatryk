package pl.imiajd.Reweda;

import java.time.LocalDate;

class Student extends Osoba
{
    public Student(String nazwisko, String[] name, LocalDate birthDay, Gender gender, String kierunek, double sredniaOcen) {
        super(nazwisko, name, birthDay, gender);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;

}