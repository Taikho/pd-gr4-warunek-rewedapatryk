package pl.imiajd.Reweda;

import java.time.LocalDate;
import java.util.Objects;

public abstract class Instrument {

    String producent;
    LocalDate rokProdukcji;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Instrument)) return false;
        Instrument that = (Instrument) o;
        return Objects.equals(getProducent(), that.getProducent()) &&
                Objects.equals(getRokProdukcji(), that.getRokProdukcji());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProducent(), getRokProdukcji());
    }

    @Override
    public String toString() {
        return String.format(producent + " +  " + rokProdukcji);
    }

    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    public abstract String dzwiek();




}
