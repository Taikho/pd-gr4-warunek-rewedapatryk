package pl.edu.uwm.wmii.rewedapatryk.laboratorium00;
import java.util.Random;
import java.util.Scanner;

public class Lab_3ex2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = 0;
        try {
            System.out.println("How many numbers do You want draw ? : ");
            n = scanner.nextInt();

        } catch (Exception e) {
            System.out.println("It must be integer number !! ");
            System.exit(1); // so i just terminate it dont want to user repair his mistake
        }

        int[] array = new int[n];
        generuj(array, n, -999, 999);
        for (int i = 0; i < n; i++) {
            System.out.println(array[i]);
        }
        System.out.println("Quantity of numbers not divided by 2: " + ileNieparzystych(array));
        System.out.println("Quantity of numbers divided by 2: " + ileParzystych(array));
        System.out.println("Quantity of numbers equal 0: " + ileZerowych(array) + ", lesser than 0: " + ileUjemnych(array) + ", more than 0: " +
                ileDodatnich(array));
        System.out.println("The highest number occurs: " + ileMaksymalnych(array) + " times" );
        System.out.println("Sum of more than 0: " + sumaDodatnich(array) + ", Sum of less than 0: " +sumaUjemnych(array));
        System.out.println("String of numbers more than 0 in a row " + dlugoscMaksymalnegoCiaguDodatnich(array));

        signum(array);


    }

    public static void generuj(int[] tab, int n, int min, int max) {
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            tab[i] = random.nextInt(max - min) + min;
        }
    }

    public static int ileNieparzystych(int[] tab) {
        int min = 0;
        for (int j : tab) {
            if (j % 2 != 0)
                min++;
        }
        return min;
    }

    public static int ileParzystych(int[] tab) {
        int plus = 0;
        for (int j : tab) {
            if (j % 2 == 0)
                plus++;
        }
        return plus;
    }

    public static int ileDodatnich(int[] tab) {
        int moreZero = 0;
        for (int j : tab) {
            if (j > 0)
                moreZero++;
        }
        return moreZero;
    }

    public static int ileUjemnych(int[] tab) {
        int lessZeros = 0;
        for (int j : tab) {
            if (j < 0)
                lessZeros++;
        }
        return lessZeros;
    }

    public static int ileZerowych(int[] tab) {
        int equalZero = 0;
        for (int j : tab) {
            if (j == 0)
                equalZero++;
        }
        return equalZero;
    }
    public static int ileMaksymalnych(int[] tab){
        int max = tab[0];
        int count = 0;
        for (int j : tab) {
            if (max <= j) {
                if (max == j)
                    count++;
                else
                    count = 1;

                max = j;
            }
        }
        return count;
    }

    public static int sumaDodatnich(int[] tab) {
        int suma = 0;
        for(int j : tab)
        {
            if(j > 0)
                suma += j;
        }
        return suma;
    }
    public static int sumaUjemnych(int[] tab)
    {
      int sumUj = 0;
      for(int i : tab)
      {
          if(i < 0)
              sumUj += i;
      }
      return sumUj;
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich (int[] tab)
    {
        int moreZero = 0, moreZeroRemember = 0;
        for(int i = 0; i < tab.length; i++)
        {
            if(tab[i] > 0) {
                moreZero++;
                for (int j = i+1; j < tab.length; j++) {
                    if (tab[j] > 0)
                        moreZero++;
                    else
                        break;
                }
                if(moreZeroRemember < moreZero)
                    moreZeroRemember = moreZero;
            }
            moreZero = 0;
        }
        return moreZeroRemember;
    }
    public static void signum(int[] tab)
    {
        for(int i = 0; i < tab.length; i++)
        {
            if(tab[i] < 0)
                tab[i] = -1;
            else
                tab[i] = 1;

            System.out.println(tab[i]);

        }
    }
}





