package pl.edu.uwm.wmii.rewedapatryk.laboratorium00;
import java.util.Random;
import java.util.Scanner;

public class Lab_3ex1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 0;
         try {
             System.out.println("How many numbers do You want draw ? : ");
             n = scanner.nextInt();

         } catch (Exception e) {
             System.out.println("It must be integer number !! ");
             System.exit(1); // so i just terminate it dont want to user repair his mistake
         }
         int[] array = new int[n];
         int even = 0; // liczba parzysta
        int odd = 0; // liczba nieparzysta
        int numZero = 0, numLessZero = 0, numMoreZero = 0; // do podpunktu B
        int max = 0, count = 0; // do podpunktu C
        int sum = 0, minusSum = 0; // do podpunktu D
        int moreZero = 0, moreZeroRemember = 0;


        for(int i = 0; i < n; i++)
         {
             array[i] = getRandomNumb(-999, 1000);
             if(array[i]%2 == 0)
                 even++;
             else
                 odd++;
             if(array[i] == 0)
                 numZero++;

             else if(array[i] < 0)
             {
                 numLessZero++;
                 minusSum += array[i];
             }
             else
             {
                 sum += array[i];
                 numMoreZero++;
             }

             if(i == 0)
                 max = array[0];

             else if(max <= array[i])
             {
                 if(max == array[i])
                     count++;
                 else
                     count = 1;

                 max = array[i];

             }

             System.out.println(array[i]);

         }

        for(int i = 0; i < n; i++)
        {
            if(array[i] > 0) {
                moreZero++;
                for (int j = i+1; j < n; j++) {
                    if (array[j] > 0)
                        moreZero++;
                    else
                        break;
                }
                if(moreZeroRemember < moreZero)
                    moreZeroRemember = moreZero;
            }
            moreZero = 0;
        }

        System.out.println("  even is 1 , odd is -1: ");

        for(int i = 0; i <n; i++)
        {
            if(array[i] < 0)
                array[i] = -1;
            else
                array[i] = 1;

            System.out.println(array[i]);
        }


         System.out.println("Quantity of even numbers: : " + even + " Count of odd numbers: " + odd);
        System.out.println("Quantity of zeros: : " + numZero + " Count of less then zero: " + numLessZero + " Count of more then zero: " + numMoreZero);
        System.out.println("Max number is: " + max + " and it is " + count + " times");
        System.out.println("Sum of numbers worth more than 0 is: " + sum + " Sum of numbers worth less than 0 is : " + minusSum);
        System.out.println("Quantity of numbers more than 0 in a row: " + moreZeroRemember);

    }
    public static int getRandomNumb(int min, int max)
    {
        Random random = new Random();
        return random.nextInt(max - min ) + min;
    }

}
