package pl.imiajd.Reweda;

import java.time.LocalDate;

public class Komputer implements Cloneable, Comparable {

    public Komputer(String c_nazwa, LocalDate c_dataProdukcji){
        nazwa = c_nazwa;
        dataProdukcji = c_dataProdukcji;
    }

    private String nazwa;
    private LocalDate dataProdukcji;


    public int compareTo(Komputer o) {
        var nazwaCompare = nazwa.compareTo(o.nazwa);
        if(nazwaCompare == 0){
            return dataProdukcji.compareTo(o.dataProdukcji);
        }

        return nazwaCompare;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false; // Jeżeli drugi nie istnieje, false
        if(obj.getClass().equals(getClass())){ // Jeżeli drugi nie jest komputerem, false (uwaga nie uwzględnia laptopów)
            var k = (Komputer)obj; // Konwertuj typ
            if(!k.nazwa.equals(nazwa)) return false; // Sprawdź nazwę
            return k.dataProdukcji.equals(dataProdukcji); // Sprawdź datę produkcji
        }
        else{
            return false; // Jeżeli klasy są różne, false
        }
    }

    @Override
    protected Object clone() {
        return new Komputer(nazwa, dataProdukcji);
    }

    public void setNazwa(String value){
        nazwa = value;
    }

    public void setDataProdukcji(LocalDate value){
        dataProdukcji = value;
    }

    @Override
    public int compareTo(Object o) {
        if(o.getClass() == Komputer.class || o.getClass() == Laptop.class) {
            return compareTo((Komputer) o);
        }
        else{
            return 0;
        }
    }

    protected LocalDate getDataProdukcji() {
        return dataProdukcji;
    }

    protected String getNazwa() {
        return nazwa;
    }
}
