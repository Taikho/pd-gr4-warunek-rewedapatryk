package pl.imiajd.Reweda;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;



public class Main {



    public static void main(String[] args) {

        var grupa = new ArrayList<Komputer>();


        grupa.add(new Komputer("Acer Predator Orion 3000", LocalDate.parse("2020-08-13")));
        grupa.add(new Komputer("Acer Predator Orion 3000", LocalDate.parse("2020-03-13")));
        grupa.add(new Komputer("Acer Predator Orion 2000", LocalDate.parse("2020-03-11")));
        grupa.add(new Komputer("Acer Predator Orion 1500", LocalDate.parse("2020-03-11")));
        grupa.add(new Komputer("Acer Predator Orion 1000", LocalDate.parse("2020-01-15")));

        System.out.println("Komputery");
        System.out.println("Przed sortowaniem: ");
        WyswietlKomputery(grupa);


        grupa.sort(Komputer::compareTo);

        System.out.println("Po sortowaniu: ");
        WyswietlKomputery(grupa);

        var grupaLaptopow = new ArrayList<Laptop>();
        grupaLaptopow.add(new Laptop("MacBook Air Late 2020", LocalDate.parse("2020-09-18"), true));
        grupaLaptopow.add(new Laptop("MacBook Air Late 2020", LocalDate.parse("2020-09-16"), true));
        grupaLaptopow.add(new Laptop("Dell Latitude", LocalDate.parse("2020-09-16"),false));
        grupaLaptopow.add(new Laptop("MacBook Pro Early 2020", LocalDate.parse("2020-03-13"), true));
        grupaLaptopow.add(new Laptop("Macbook Pro Late 2020", LocalDate.parse("2020-09-16"), true));

        System.out.println("Laptopy");
        System.out.println("Przed sortowaniem: ");
        WyswietlLaptopy(grupaLaptopow);

        System.out.println("Po sortowaniu: ");
        grupaLaptopow.sort(Laptop::compareTo);
        WyswietlLaptopy(grupaLaptopow);
    }

    public static void WyswietlKomputery(ArrayList<Komputer> list){
        for(Komputer komputer : list) {
            System.out.println("\t" + komputer.getNazwa() + " wyprodukowany " + komputer.getDataProdukcji().toString());
        }

    }

    public static void WyswietlLaptopy(ArrayList<Laptop> list){
        for(Laptop komputer : list) {
            System.out.println("\t" + komputer.getNazwa() + " wyprodukowany " + komputer.getDataProdukcji().toString());
        }



    }



    public static void redukuj(LinkedList<String> komputery)
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Ktory element chcesz usunac ?: ");
        int n = scan.nextInt();
        komputery.remove(n);

        //nigdy nie uzywalem Linked listy



    }

}