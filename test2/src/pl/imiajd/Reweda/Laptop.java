package pl.imiajd.Reweda;

import java.time.LocalDate;

public class Laptop extends Komputer implements Cloneable, Comparable {

    private boolean czyApple;

    public Laptop(String c_nazwa, LocalDate c_dataProdukcji, boolean c_czyApple) {
        super(c_nazwa, c_dataProdukcji);
        czyApple = c_czyApple;
    }

    public int compareTo(Laptop o) {
        var resCompareKomputer = compareTo((Komputer) o);
        if(resCompareKomputer == 0){
            if(czyApple != o.czyApple){
                if(czyApple == true) return 1;
                return -1;
            }
            return 0;
        }
        return resCompareKomputer;
    }

    @Override
    protected Object clone() { // Nie wspiera deep-cloning
        var obj = new Laptop(getNazwa(), getDataProdukcji(), czyApple);
        return obj;
    }

    @Override
    public int compareTo(Object o) {
        if(o.getClass() == Laptop.class){
            return compareTo((Laptop) o);
        }
        else{
            if(o.getClass() == Komputer.class)
                return compareTo((Komputer) o);
            else
                return 0;
        }
    }

    public void setCzyApple(boolean value){
        czyApple = value;
    }

}
