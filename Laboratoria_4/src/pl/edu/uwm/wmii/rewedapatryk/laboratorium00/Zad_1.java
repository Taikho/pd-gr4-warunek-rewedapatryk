package pl.edu.uwm.wmii.rewedapatryk.laboratorium00;

public class Zad_1 {
    public static void main(String[] args) {

        System.out.println("a) " + countChar("comcmon", 'c'));
        System.out.println("b) " + countSubStr("rimcimrimpamcimpam", "rim"));
        System.out.println("c) " + middle("JOHNY"));
        System.out.println("d) " + repeat("HO",4));

    }

    public static int countChar(String str, char c) {

        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                count++;
            }
        }
        return count;

    }

    public static int countSubStr(String str, String subStr)
    {
        int index = 0, count = 0;
        while (true) {
            index = str.indexOf(subStr, index);
            if (index != -1) {
                count ++;
                index += subStr.length();
            } else {
                break;
            }
        }

        return count;

        }

        public static String middle(String str)
        {
            int leng = str.length();
            if(leng%2 == 0)
            {
                return str.substring((leng/2)-1, (leng/2)+1);
            }
            else
                return str.substring(leng/2, (leng/2)+1);


        }
        public static String repeat(String str, int n)
        {

            String repeated = str.repeat(n);
            return repeated;

        }




}
