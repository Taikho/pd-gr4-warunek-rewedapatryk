package pl.edu.uwm.wmii.rewedapatryk.laboratorium00;

import java.util.ArrayList;
import java.util.Collections;

public class Main {

	public static void main(String[] args) {
		ArrayList<Integer> a = new ArrayList<>();
		ArrayList<Integer> b = new ArrayList<>();

		a.add(1);
		a.add(4);
		a.add(9);
		a.add(16);
		b.add(9);
		b.add(7);
		b.add(4);
		b.add(9);
		b.add(11);

		System.out.println(append(a, b));
		System.out.println(merge(a, b));
		System.out.println(mergeSorted(a, b));
		System.out.println(reversed(a));
		reverse(a);
		System.out.println(a);

	}

	public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b) {
		ArrayList<Integer> c = new ArrayList<>(a);
		c.addAll(b);
		return c;

	}

	public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
		int checkA = 0, checkB = 0;
		ArrayList<Integer> c = new ArrayList<>();

		while (checkA < a.size() || checkB < b.size()) {
			if (checkA < a.size()) {
				c.add(a.get(checkA));
				checkA++;
			}

			if (checkB < b.size()) {
				c.add(b.get(checkB));
				checkB++;
			}

		}
		return c;
	}

	public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {

		ArrayList<Integer> c = new ArrayList<>(merge(a, b));
		Collections.sort(c);
		return c;

	}

	public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
		ArrayList<Integer> c = new ArrayList<>();
		int check = a.size() - 1;

		while (check  > -1 ) {
			c.add(a.get(check));
			check--;

		}
		return c;

	}
	public static void reverse(ArrayList<Integer> a)
	{
		int check = a.size() - 1;

		while (check  > -1 ) {
			a.add(a.get(check));
			a.remove(check);
			check--;


		}
	}

}
