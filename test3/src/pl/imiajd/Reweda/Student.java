package pl.imiajd.Reweda;
import java.time.LocalDate;
import java.util.ArrayList;

public class Student implements Cloneable, Comparable {


    private String nazwa;
    private LocalDate dataUrodzenia;
    private double ocena;

    public Student(String c_nazwa, LocalDate c_dataUrodzenia, double c_ocena) {
        nazwa = c_nazwa;
        dataUrodzenia = c_dataUrodzenia;
        ocena = c_ocena;
    }


    public String toString()
    {
        return getClass()+" ["+dataUrodzenia+"] "+ dataUrodzenia+ " nazwa: "+ nazwa+ " ocena: " + ocena ;

    }

    protected LocalDate getdataUrodzenia() {
        return dataUrodzenia;
    }

    protected String getNazwa() {
        return nazwa;
    }

    protected double getocena() {
        return ocena;
    }


    public int compareTo(Student T)
    {
        if(this.dataUrodzenia==T.dataUrodzenia && this.nazwa == T.nazwa )
        {
            return 1;
        }
        else
            return 0;
    }



}