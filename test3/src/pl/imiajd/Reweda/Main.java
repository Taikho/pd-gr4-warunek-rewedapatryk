package pl.imiajd.Reweda;
import java.time.LocalDate;
import java.util.ArrayList;


public class Main {

    public static void main(String[] args) {

        var grupa = new ArrayList<Student>();


        grupa.add(new Student("Dominik", LocalDate.parse("1998-08-13"), 3.5));
        grupa.add(new Student("Mateusz", LocalDate.parse("1995-03-13"), 4));
        grupa.add(new Student("Kamil", LocalDate.parse("2001-03-11"), 2));
        grupa.add(new Student("Kamila", LocalDate.parse("2003-03-11"), 5));
        grupa.add(new Student("Marcin", LocalDate.parse("2004-01-15"), 3));



        System.out.println("Studenci");
        System.out.println("Przed sortowaniem: ");

        for (Student x: grupa) {
            System.out.println(x.toString());
        }


        System.out.println("Po sortowaniu: ");
        grupa.sort(Student::compareTo);
        for (Student x: grupa) {
            System.out.println(x.toString());
        }



        /* tu jest zadanie 2 ktorego nie skonczylem

        static double kwotaStypendium = 0;

        public static void setKwotaStypendium(double kwotaStypendium) {
            for(Stundent s : lista) {
                if(s.dataUrodzenia.year > 2005 & s.ocena == 5) {
                    this.kwotaStypendium = 500;
                }
            }
        }

*/




    }
}
